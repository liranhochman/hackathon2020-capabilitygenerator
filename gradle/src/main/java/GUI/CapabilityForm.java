package main.java.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import javax.swing.*;
import main.java.ConnectToCloud;
import org.json.JSONArray;
import org.json.JSONObject;

//https://www.google.com/search?q=intellij+idea+create+gui+form&rlz=1C1GCEU_en-GBIL927IL927&oq=intellij+java+how+to+create+gui&aqs=chrome.2.69i57j0i22i30i457j0i22i30.14022j0j7&sourceid=chrome&ie=UTF-8#kpvalbx=_0LjhX-X8MtjzkgWfoaGoAg13
public class CapabilityForm {
    public JPanel panelMain;
    private JFormattedTextField urlTextField;
    private JTextField AccessKeyTextField;
    private JButton connectButton;
    private JButton disconnectButton;

    private static String cloudUrl;
    private static String accessKey;

    public CapabilityForm() {

        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cloudUrl = urlTextField.getText();
                accessKey = AccessKeyTextField.getText();

                ConnectToCloud ctc = new ConnectToCloud();
                boolean connectionStatus = ctc.checkConnection();
                JOptionPane.showMessageDialog(null, "connection status: "+ connectionStatus);

                if (connectionStatus) {
                    JFrame frame2 = new JFrame("CapabilityGenerator");
                    frame2.setVisible(true);

//                    JFrame frame = new JFrame("CapabilityGenerator");
                    frame2.setContentPane(new CapabilityGenerator().panelMain);
                    frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame2.pack();
                    frame2.setVisible(true);

                }
            }
        });

    }


    public static String getCloudUrl() {
        return cloudUrl;
    }

    public static String getAccessKey() {
        return accessKey;
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("CapabilityForm");
        JFrame frame2 = new JFrame("CapabilityGenerator");
        frame.setContentPane(new CapabilityForm().panelMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


    }
}
