package main.java.GUI;

import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import main.java.BrowsersParams;
import main.java.CapabilityConfig;
import main.java.ConnectToCloud;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.swing.*;

public class CapabilityGenerator {
    public JPanel panelMain;
    private JTextArea browserNameTextArea;
    private JComboBox BrowserNameComboBox;
    private JTextArea OSTextArea;
    private JComboBox OsComboBox;
    private JTextArea browserVersionTextArea;
    private JComboBox BrowserVersionComboBox;
    private JComboBox CapabilityList_Closed;
    private JComboBox CapabilityListOptions;
    private JComboBox CapabilityList_open;
    private JTextArea capabilitiesGeneratorTextArea;
    private JTextArea agentNameTextArea;
    private JComboBox AgentNameComboBox;
    private JTextArea platformTextArea;
    private JComboBox PlatformComboBox;
    private JTextPane regionTextPane;
    private JComboBox RegionComboBox;
    private JButton GenerateBTN;
    private JTextField CapabilityStringValues;
    private JButton RefreshBTN;
    private JTextPane CapabilitiesResult;
    private CapabilityConfig cc;
    private HashMap<String, ArrayList<String>> capabilitiesWithValues;
    private ArrayList<String> capabilitiesWithOpenFields;

    public static ArrayList browsersList;
    public static HashMap<String, String> filterBrowsersCapabilities;

    public CapabilityGenerator() {
        cc = new CapabilityConfig();
        capabilitiesWithValues = cc.getCapabilityValues();
        capabilitiesWithOpenFields = cc.getCapabilitiesOpenFields();

        filterBrowsersCapabilities = new HashMap<>();

        browsersList = ConnectToCloud.getBrowserList();
        /******* NOT Browsers dropDown******/
        addPopupMenuListener (CapabilityList_Closed , null, new ArrayList<String>(capabilitiesWithValues.keySet()));
        addPopupMenuListener (CapabilityList_open ,null , capabilitiesWithOpenFields);


        /*******Browsers dropDown******/

        /**BrowserNameComboBox*/
        //fill the list and save the chosen value
        ArrayList finalParamList =
                BrowsersParams.getBrowserCapabilityBeFilter(browsersList,
                        BrowsersParams.BROWSERNAME, filterBrowsersCapabilities);
        addPopupMenuListener(BrowserNameComboBox, BrowsersParams.BROWSERNAME, finalParamList);
        /**OsComboBox*/
        finalParamList =
                BrowsersParams.getBrowserCapabilityBeFilter(browsersList,
                        BrowsersParams.OSNAME, filterBrowsersCapabilities);
        addPopupMenuListener(OsComboBox, BrowsersParams.OSNAME, finalParamList);

        /**BROWSERVERSION*/
        finalParamList =
                BrowsersParams.getBrowserCapabilityBeFilter(browsersList,
                        BrowsersParams.BROWSERVERSION, filterBrowsersCapabilities);
        addPopupMenuListener(BrowserVersionComboBox, BrowsersParams.BROWSERVERSION, finalParamList);

        /**PLATFORM*/
        finalParamList =
                BrowsersParams.getBrowserCapabilityBeFilter(browsersList,
                        BrowsersParams.PLATFORM, filterBrowsersCapabilities);
        addPopupMenuListener(PlatformComboBox, BrowsersParams.PLATFORM, finalParamList);

        /**AGENTNAME*/
        finalParamList =
                BrowsersParams.getBrowserCapabilityBeFilter(browsersList,
                        BrowsersParams.AGENTNAME, filterBrowsersCapabilities);
        addPopupMenuListener(AgentNameComboBox, BrowsersParams.AGENTNAME, finalParamList);

        /**REGIONNAME*/
        finalParamList =
                BrowsersParams.getBrowserCapabilityBeFilter(browsersList,
                        BrowsersParams.REGIONNAME, filterBrowsersCapabilities);
        addPopupMenuListener(RegionComboBox, BrowsersParams.REGIONNAME, finalParamList);

        RefreshBTN.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    try {  addCapability(BrowsersParams.BROWSERNAME, BrowserNameComboBox.getSelectedItem().toString()); }catch (Exception e1){}
                        try {   addCapability(BrowsersParams.BROWSERVERSION, BrowserVersionComboBox.getSelectedItem().toString()); }catch (Exception e1){}
                            try {    addCapability(BrowsersParams.PLATFORM, PlatformComboBox.getSelectedItem().toString()); }catch (Exception e1){}
                                try {    addCapability(BrowsersParams.OSNAME, OsComboBox.getSelectedItem().toString()); }catch (Exception e1){}
                                    try {    addCapability(BrowsersParams.AGENTNAME, AgentNameComboBox.getSelectedItem().toString()); }catch (Exception e1){}
                                        try {     addCapability(BrowsersParams.REGIONNAME, RegionComboBox.getSelectedItem().toString()); }catch (Exception e1){}
                                            try {    addCapability(CapabilityList_Closed.getSelectedItem().toString(),
                            CapabilityListOptions.getSelectedItem().toString()); }catch (Exception e1){}
                                                try {      addCapability(CapabilityList_open.getSelectedItem().toString(),
                            CapabilityStringValues.getSelectedText());}catch (Exception e1){}
                }catch (Exception e1){}
            }
        });

        GenerateBTN.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String finalCP = generateCapabilities(new JSONObject(cc.generateCapabilities()));
//                JOptionPane.showMessageDialog(null, finalCP);
                CapabilitiesResult.setText(finalCP);

            }
        });
    }


    public void addCapability(String capabilityName, String value){
        cc.changeCapabilitiesListToGenerate(
                capabilityName, value);
        filterBrowsersCapabilities.put(capabilityName, value);
    }

    public void addPopupMenuListener(JComboBox comboBox, String capabilityName, ArrayList listOfValuse) {
        comboBox.addPopupMenuListener(new

      PopupMenuListener() {
          @Override
          public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
              if (listOfValuse != null) {
                  fillComboList(comboBox, listOfValuse);
              }
          }

          @Override
          public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
//              stateChanged(capabilityName);
                  comboBox.addItemListener(new ItemListener() {
                      @Override
                      public void itemStateChanged(ItemEvent e) {
                          if (capabilityName != null) {
                              String value = e.getItem().toString();
                              cc.changeCapabilitiesListToGenerate(
                                      capabilityName, value);
                              filterBrowsersCapabilities.put(capabilityName, value);
                              System.out.println(value);
                          } else { //if we just open the left capability list
                              // open the right capability list
                              String key = e.getItem().toString();
                              addPopupMenuListener (CapabilityListOptions, key , capabilitiesWithValues.get(key) );
                          }
                      }
                  });

//              } else {
//                  comboBox.addItemListener(new ItemListener() {
//                      @Override
//                      public void itemStateChanged(ItemEvent e) {
//                         // create the second list
//                          String key = e.getItem().toString();
//                          addPopupMenuListener (CapabilityListOptions, capabilitiesWithValues.get(key));
//                      }
//                  });
//              }
          }

          @Override
          public void popupMenuCanceled(PopupMenuEvent e) {
          }
      });
    }

    public void fillComboList(JComboBox comboBox, ArrayList<String> list) {
        comboBox.removeAllItems();
        comboBox.addItem("");
        for (int i = 0; i < list.size(); i++) {
            comboBox.addItem(list.get(i));
        }
    }

    public static String generateCapabilities(JSONObject jsonObject) {
        String capabilitiesStr = "";
        try{
            Iterator<String> keys = jsonObject.keys();
            while(keys.hasNext()) {
                String key = keys.next();
                Object value = jsonObject.get(key);
                try{
                    int num = Integer.parseInt(value.toString());
                    // is an integer!
                    capabilitiesStr += "dc.setCapability(" + key +", " + num + ");" + System.lineSeparator();
                }
                catch (NumberFormatException e) {
                    // not an integer!
                    if(value.equals("false")||value.equals("true")){
                        capabilitiesStr += "dc.setCapability(" + key +", " + value + ");" + System.lineSeparator();
                    }
                    else{
                        capabilitiesStr += "dc.setCapability(" + key +", " + "\"" + value + "\");" + System.lineSeparator();
                    }
                }
            }
        }
        catch (NullPointerException e)
        {
            return capabilitiesStr;
        }

        return capabilitiesStr;
    }



    public static void main(String[] args) {
        String url = "https://tokyo.experitest.com";
        String accessKey =
                "eyJ4cC51Ijo0OSwieHAucCI6MSwieHAubSI6Ik1UVTROVFU0TkRjeU16YzJNUSIsImFsZyI6IkhTMjU2In0.eyJleHAiOjE5MDA5NDQ3MjMsImlzcyI6ImNvbS5leHBlcml0ZXN0In0.9GLkD93r2u1DWAh6_G0LrmHp_QEX4bmvKPVszV4VWIc";
        ConnectToCloud ctc = new ConnectToCloud(url, accessKey);

        ///////////////

        JFrame frame = new JFrame("CapabilityGenerator");
        frame.setContentPane(new CapabilityGenerator().panelMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}


