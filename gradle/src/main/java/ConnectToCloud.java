package main.java;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import java.util.ArrayList;
import java.util.HashMap;
import main.java.GUI.CapabilityForm;
import org.json.JSONArray;
import org.json.JSONObject;

public class ConnectToCloud {
    protected static String cloudURL;
    protected static String accessKey;
    private static HttpResponse<String> responseString;

    public ConnectToCloud(){
        this.cloudURL = CapabilityForm.getCloudUrl();
        this.accessKey = CapabilityForm.getAccessKey();
    }

    public ConnectToCloud(String cloudURL, String accessKey){
        this.cloudURL = cloudURL;
        this.accessKey = accessKey;
    }

    public boolean checkConnection(){
        HttpResponse response= getHttpResponse();
        if(response != null) {
            Integer statusNum = response.getStatus();
            if (statusNum == 200)
                return true;
        }
        return false;
    }

    public HttpResponse<String> getHttpResponse() {
        try {
            responseString = Unirest.get(this.cloudURL)
                    .header("Authorization", "Bearer " + this.accessKey)
                    .asString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseString;
    }

    public static JSONArray getAllBrowsers() {
        String url = cloudURL + "/api/v1/browsers";
        try {
            responseString = Unirest.get(url)
                    .header("Authorization", "Bearer " + accessKey)
                    .asString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (new JSONObject(responseString.getBody())).getJSONArray("data");
    }

    public static ArrayList getBrowserList(){
        JSONArray browsersList = getAllBrowsers();
        ArrayList browsers = new ArrayList<>();
        for (int i = 0; i < browsersList.length(); i++) {
            BrowsersParams bp = new BrowsersParams(
                    ((JSONObject) browsersList.get(i)).getString("browserName"),
                    ((JSONObject) browsersList.get(i)).getString("browserVersion"),
                    ((JSONObject) browsersList.get(i)).getString("platform"),
            ((JSONObject) browsersList.get(i)).getString("osName"),
                    ((JSONObject) browsersList.get(i)).getString("agentName"),
            ((JSONObject) browsersList.get(i)).getString("region")
            );
                browsers.add(bp);
        }
        return browsers;
    }


    public static void main(String[] args) {
        String url = "https://tokyo.experitest.com";
        String accessKey = "eyJ4cC51Ijo0OSwieHAucCI6MSwieHAubSI6Ik1UVTROVFU0TkRjeU16YzJNUSIsImFsZyI6IkhTMjU2In0.eyJleHAiOjE5MDA5NDQ3MjMsImlzcyI6ImNvbS5leHBlcml0ZXN0In0.9GLkD93r2u1DWAh6_G0LrmHp_QEX4bmvKPVszV4VWIc";
        ConnectToCloud ctc = new ConnectToCloud(url ,accessKey);
        boolean connectionStatus = ctc.checkConnection();
//        JSONArray getAllBrowsers = ctc.getAllBrowsers();
        ArrayList browsers = getBrowserList();

        HashMap filters = new HashMap<>();
//        filters.put("browserName","MicrosoftEdge");
//        filters.put("browserName","safari");
        filters.put("browserName","safari");
        filters.put("browserVersion","12.1.2");
//        ArrayList capabilityList = browsersParams.getBrowserCapabilityByFilter(browsers, "agentName", filters);
    }

}
