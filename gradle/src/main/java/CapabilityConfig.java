package main.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CapabilityConfig {
    private HashMap<String, String> capabilities;
    private HashMap<String, ArrayList<String>> capabilityValues;
    private ArrayList<String> capabilitiesOpenFields;
    private HashMap<String, String> generateCapabilities;


    public CapabilityConfig(){
        createCapabilityList();
        addCapabilityValues();
        createCapabilityListOpenFields();
    }

    public HashMap<String, String> getCapabilitiesListToGenerate() {
        return capabilities;
    }

    public void changeCapabilitiesListToGenerate(String key, String value) {
        capabilities.replace(key,value);
    }

    public HashMap<String, ArrayList<String>> getCapabilityValues() {
        return capabilityValues;
    }

    public ArrayList<String> getCapabilitiesOpenFields() {
        return capabilitiesOpenFields;
    }
    
    public HashMap<String, String> generateCapabilities() {
        generateCapabilities = new HashMap<>();
        for (Map.Entry<String, String> entry : capabilities.entrySet()) {
            if(!entry.getValue().isEmpty()){
                generateCapabilities.put(entry.getKey(), entry.getValue());
            }
        }
        return generateCapabilities;
    }


    private void createCapabilityList(){
        capabilities = new HashMap<>();
        capabilities.put("browserName", "");
        capabilities.put("browserVersion", "");
        capabilities.put("osName", "");
        capabilities.put("version", "");
        capabilities.put("platform", "");
        capabilities.put("agentName", "");
        capabilities.put("region", "");
        capabilities.put("takeScreenshots", "");
        capabilities.put("seleniumScreenshot", "");
        capabilities.put("generateReport", "");
        capabilities.put("generateVideoForReport", "");
        capabilities.put("handlesAlerts", "");
        capabilities.put("newSessionWaitTimeout", "");
        capabilities.put("newCommandTimeout", "");
        capabilities.put("maxScreenshotInterval", "");
        capabilities.put("manualSessionTimeoutInMillis", "");
        capabilities.put("username", "");
        capabilities.put("projectName", "");
        capabilities.put("cloudKey", "");
        capabilities.put("sessionId", "");
        capabilities.put("manualUserNumber", "");
        capabilities.put("reportType", "");
        capabilities.put("tunnelAddress", "");
        capabilities.put("tunnelGatewayAddress", "");
        capabilities.put("tunnelStatus", "");
        capabilities.put("tunnelClientDownloadUrl", "");
        capabilities.put("tunnelingCapabilities", "");
        capabilities.put("reporterInfo", "");
        capabilities.put("closePopups", "");
        capabilities.put("chromeOptions", "");
        capabilities.put("goog:chromeOptions", "");
        capabilities.put("edgeOptions", "");
        capabilities.put("ms:edgeOptions", "");
    }

    private void addCapabilityValues() {
        capabilityValues = new HashMap<>();
        ArrayList<String> yesNoValues = new ArrayList<>();
        yesNoValues.add("true");
        yesNoValues.add("false");
        capabilityValues.put("takeScreenshots", yesNoValues);
        capabilityValues.put("seleniumScreenshot", yesNoValues);
        capabilityValues.put("generateReport", yesNoValues);
        capabilityValues.put("generateVideoForReport", yesNoValues);
        capabilityValues.put("handlesAlerts", yesNoValues);
        capabilityValues.put("reportType", new ArrayList<String>(){
            {
                add("video");
                add("screenshot");
                add("both"); }
        });
    }

    private void createCapabilityListOpenFields() {
        capabilitiesOpenFields = new ArrayList<String>(){
            {
                add("newSessionWaitTimeout");
                add("newCommandTimeout");
                add("maxScreenshotInterval");
                add("manualSessionTimeoutInMillis");
                add("username");
                add("projectName");
                add("cloudKey");
                add("sessionId");
                add("manualUserNumber");
                add("tunnelAddress");
                add("tunnelGatewayAddress");
                add("tunnelStatus");
                add("tunnelClientDownloadUrl");
                add("tunnelingCapabilities");
                add("reporterInfo");
                add("closePopups");
                add("chromeOptions");
                add("goog:chromeOptions");
                add("edgeOptions");
                add("ms:edgeOptions");
            }};
    }
}