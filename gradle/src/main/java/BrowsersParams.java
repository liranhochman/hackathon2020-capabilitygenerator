package main.java;

import java.util.*;

public class BrowsersParams {
    public static final String BROWSERNAME = "browserName";
    public static final String BROWSERVERSION = "browserVersion";
    public static final String PLATFORM = "platform";
    public static final String OSNAME = "osName";
    public static final String AGENTNAME = "agentName";
    public static final String REGIONNAME = "region";

    private String browserName;
    private String browserVersion;
    private String platform;
    private String osName;
    private String agentName;
    private String region;

    public BrowsersParams(String browserName, String browserVersion, String platform, String osName, String agentName, String region){
        this.browserName = browserName;
        this.browserVersion = browserVersion;
        this.platform = platform;
        this.osName = osName;
        this.agentName = agentName;
        this.region = region;
    }


    public String getBrowserName() {
        return browserName;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public String getPlatform() {
        return platform;
    }

    public String getOsName() {
        return osName;
    }

    public String getAgentName() {
        return agentName;
    }

    public String getRegion() {
        return region;
    }

    public String getCapability(String keyWanted) {
        switch (keyWanted) {
            case "browserName":
                return getBrowserName();
            case "browserVersion":
                return getBrowserVersion();
            case "platform":
                return getPlatform();
            case "osName":
                return getOsName();
            case "region":
                return getRegion();
            case "agentName":
                return getAgentName();
        }
        return null;
    }

    public static ArrayList getBrowserCapabilityBeFilter(ArrayList<BrowsersParams> browsers, String keyWanted,
                                                         HashMap<String, String> filters) {
        ArrayList<String> keysResult = new ArrayList<>();
        boolean addTheKey = true;
        for (BrowsersParams browser : browsers) {
            if (filters != null) {
                for (Map.Entry<String, String> filter : filters.entrySet()) {
                    if (!browser.getCapability(filter.getKey()).equals(filter.getValue())){
                        addTheKey = false;
                    }
                }
            }
            if (addTheKey){
                keysResult.add(browser.getCapability(keyWanted));
            }
            addTheKey = true;
        }

        ArrayList<String> listWithoutDuplicates = new ArrayList<>(
                new HashSet<>(keysResult));
        return listWithoutDuplicates;
    }

}
